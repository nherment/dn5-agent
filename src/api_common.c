
#include <string.h>
#include <curl/curl.h>
#include <json-c/json.h>

#include "api_common.h"
#include "logger.h"

size_t parse_body(char *buffer, size_t size, size_t nmemb, http_response *response) {
  
  if(response->body == NULL) {
    response->body = malloc(MAX_BODY_SIZE * sizeof(char));
  }
  
  size_t char_count = size * nmemb;
  
  if((strlen(response->body) + char_count) < MAX_BODY_SIZE) {
    strncat(response->body, buffer, char_count);
    return char_count;
  }
  
  return 0;
}

void post(char* url_path, const char* query, http_response* response) {
  CURL *curl;
  CURLcode res;

  char url[256];
  long status_code = 0;
  url[0] = '\0';
  
  strcat(url, DN5_REMOTE_API_URL);
  strcat(url, url_path);
  
  /* get a curl handle */
  curl_global_init(CURL_GLOBAL_DEFAULT);
  curl = curl_easy_init();
  struct curl_slist *curl_headers = NULL;
  if(curl) {
    
    curl_headers = curl_slist_append(curl_headers, "Content-Type: application/json");
    curl_headers = curl_slist_append(curl_headers, "Accept: application/json");
 
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, curl_headers);
    
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, query);
    
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, parse_body);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    
    res = curl_easy_perform(curl);
    if(res != CURLE_OK) {
      log_error("POST(%s) failed: %s\n", url, curl_easy_strerror(res));
    }
    
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status_code);
    
    response->status_code = status_code;

    curl_easy_cleanup(curl);
  }

}

int callout(char* url, const char* query) {

  http_response response;
  response.status_code = 0;
  response.header_count = 0;
  response.headers = NULL;
  response.body = NULL;
  
  post(url, query, &response);
  if(response.status_code == 200) {
    json_object *jbody = json_tokener_parse(response.body);
    json_object *jstatus = json_object_new_object();
    if(json_object_object_get_ex(jbody, "status", &jstatus)) {
      int status = json_object_get_int(jstatus);
      return status;
    } else {
      log_error("Server response format from [%s] is not recognized.\n%s", url, response.body);
      return -1;
    }
  } else {
    log_error("HTTP callout to [%s] failed.\n%s", url, response.body);
    return -2;
  }
}
