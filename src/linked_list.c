
#include "linked_list.h"

linked_list_t* linked_list_create() {
  linked_list_t* linked_list = (linked_list_t*)malloc(sizeof(linked_list_t));
  linked_list->length = 0;
  linked_list->head = NULL;
  linked_list->tail = NULL;
  return linked_list;
}

void linked_list_push(linked_list_t* linked_list, void* element) {

  linked_list_node_t* new_node = (linked_list_node_t*)malloc(sizeof(linked_list_node_t));

  new_node->next = NULL;
  new_node->previous = NULL;
  
  new_node->element = element;
  
  if(linked_list->tail == NULL) {
    linked_list->tail = new_node;
  } else {
    linked_list->tail->next = new_node;
    new_node->previous = linked_list->tail;
  }
  if(linked_list->head == NULL) {
    linked_list->head = new_node;
  }

  linked_list->length++;
  
}

void* linked_list_pop(linked_list_t* linked_list) {

  if(linked_list->length > 0) {
    void* element = linked_list->tail->element;
    linked_list_node_t* removed_node = linked_list->tail;
    if(linked_list->length == 1) {
      linked_list->head = linked_list->tail = NULL;
    }
    linked_list->tail = linked_list->tail->previous;
    free(removed_node);
    return element;
  } else {
    return NULL;
  }
  
}

void* linked_list_find(linked_list_t* linked_list, linked_list_iterator func) {
  linked_list_node_t* pointer = linked_list->head;
  while(pointer != NULL) {
    bool found = func(pointer->element);
    if(found) {
      return pointer->element;
    }
    pointer = pointer->next;
  }

  return NULL;
  
}

void* linked_list_shift(linked_list_t* linked_list) {
  return NULL;
}

void* linked_list_unshift(linked_list_t* linked_list) {
  return NULL;
}

void linked_list_destroy(linked_list_t* linked_list) {
}
