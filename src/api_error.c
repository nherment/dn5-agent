
#include <string.h>

#include "api_error.h"
#include "logger.h"


void api_error_register_print_message(int status) {
  switch(status) {
  case 401:
    log_error("Error %i: Email format invalid", status);
    break;
  case 402:
    log_error("Error %i: Email already registered", status);
    break;
  case 403:
    log_error("Error %i: Email pending validation", status);
    break;
  case 404:
    log_error("Error %i: Email missing", status);
    break;
  case 411:
    log_error("Error %i: Password invalid", status);
    break;
  case 414:
    log_error("Error %i: Password missing", status);
    break;
  case 421:
    log_error("Error %i: FQDN invalid", status);
    break;
  case 422:
    log_error("Error %i: FQDN already registered", status);
    break;
  case 424:
    log_error("Error %i: FQDN missing", status);
    break;
  default:
    log_error("Unrecognized status code: %i", status);
    break;
  }
}

void api_error_print_message(char* api_name, int status) {
  if(strcmp(api_name, "register") == 0) {
    api_error_register_print_message(status);
  } else {
    log_error("Unrecognized api name for printing error message. %s:%i", api_name, status);
  }
}
