
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bool.h>

#include "arguments.h"
#include "hashmap.h"
//#include "bool.h"
#include "logger.h"

#define ARGUMENTS_MAX_OPTIONS 100
#define ARGUMENT_STRING_MAX_CHARS 255

arguments_t* arguments[ARGUMENTS_MAX_OPTIONS];

void arguments_define(arguments_t* new_arguments) {
  int cnt = 0;
  while(arguments[cnt] != 0 && cnt < ARGUMENTS_MAX_OPTIONS) {
    cnt ++;
  }
  if(cnt < ARGUMENTS_MAX_OPTIONS) {
    arguments[cnt] = new_arguments;
  }
  log_debug("new action registered [%s]", new_arguments->verb);
}

void arguments_print_usage() {
  printf("Usage:\n");
  printf("dn5-agent <action> [options]\n");
  printf("\n");

  arguments_t* args = NULL;
  for(int cnt = 0 ; arguments[cnt] != 0 && cnt < ARGUMENTS_MAX_OPTIONS ; cnt++) {
    args = arguments[cnt];
    printf("%s", args->verb);
    printf("\n");
    for(int i = 0 ; i < args->count ; i++) {

      argument_t* arg = &(args->arguments[i]);
      printf("\t%s", arg->value);
      switch(arg->type) {
      case ARG_TYPE_BOOL:
        printf(" (boolean)");
        break;
      case ARG_TYPE_STRING:
        printf(" (string)");
        break;
      }
      if(!arg->required) {
        printf(" (optional)");
      }
      printf("\n");
    }
    printf("\n");
  }
  printf("\n");
}

map_t* arguments_parse(int argc, char** args) {
  map_t *arguments_map = hashmap_new();
  if(argc <= 1) {
    return arguments_map;
  }
  
  char* action_verb = args[1];
  arguments_t* expected_arguments = NULL;
  for(int cnt = 0 ; arguments[cnt] != 0 && cnt < ARGUMENTS_MAX_OPTIONS ; cnt++) {
    if(strcmp(action_verb, arguments[cnt]->verb) == 0) {
      expected_arguments = arguments[cnt];
      break;
    }
  }
  
  int error = 0;
  char* missing_args[ARGUMENTS_MAX_OPTIONS];
  int missing_args_cnt = 0;
  if(expected_arguments != 0) {
    
    hashmap_put(arguments_map, "cmd", (any_t) expected_arguments->verb);
    log_debug(">> %s", expected_arguments->verb);
    for(int i = 0 ; i < expected_arguments->count ; i++) {
      
      int has_argument = 0;
      argument_t* expected_argument = &(expected_arguments->arguments[i]);
      log_debug("Searching for arg #%i, %s", i, expected_argument->value);
      
      for(int j = 2 ; j < argc ; j++) {
        
        if(strcmp(args[j], expected_argument->value) == 0) {
          log_debug("Found arg %s", expected_argument->value);
          
          if(expected_argument->type == ARG_TYPE_BOOL) {
            has_argument = 1;
            hashmap_put(arguments_map, expected_argument->value, (any_t) "1");
            log_debug("%s -> %s", expected_argument->value, "true");
            break;
          } else if(expected_argument->type == ARG_TYPE_STRING) {
            if(j+1 < argc) {
              has_argument = 1;
              char* args_val = (char*) malloc(ARGUMENT_STRING_MAX_CHARS * sizeof(char) + 1); // hardcoded limit for args of 255 chars
              strncpy(args_val, args[j+1], ARGUMENT_STRING_MAX_CHARS);
              args_val[255] = '\0';
              hashmap_put(arguments_map, expected_argument->value, (any_t) args_val);
              log_debug("%s -> %s", expected_argument->value, args_val);
              break;
            } else {
              has_argument = 0;
              break;
            }
          }
        }
      }
      if(expected_argument->required == 1 && has_argument == 0) {
        missing_args[missing_args_cnt] = expected_argument->value;
        missing_args_cnt ++;
        error = 1;
      }
    }
  } else {
    error = 1;
  }
  
  if(error) {
    if(missing_args_cnt > 0) {
      log_error("The following arguments are required:");
      for(int i = 0 ; i < missing_args_cnt ; i++) {
        log_error("- %s", missing_args[i]);
      }
    } else {
      log_error("There was an error parsing your arguments");
    }
    arguments_print_usage();
    return NULL;
  } else {
    return arguments_map;
  }
}

void arguments_free() {
  
}
