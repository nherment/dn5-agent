
#include "logger.h"

log4c_category_t* log_cat_root = NULL;
void logger_init() {
  if(log4c_init()) {
    printf("Failed to initialized Log4C");
  } else {
    log_cat_root = log4c_category_get("dn5.agent");
    log_info("logger started");
  }
}

void logger_cleanup() {
  if ( log4c_fini()){
    printf("Failed to terminate Log4C properly");
  }
}

void log_debug(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(DEBUG, format, args);
  va_end(args);
}

void log_info(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(INFO, format, args);
  va_end(args);
}

void log_notice(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(NOTICE, format, args);
  va_end(args);
}

void log_warning(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(WARNING, format, args);
  va_end(args);
}

void log_error(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(ERROR, format, args);
  va_end(args);
}

void log_critical(const char* format, ...) {
  va_list args;
  va_start(args, format);
  log_level(CRITICAL, format, args);  
  va_end(args);
}

log4c_priority_level_t transform_log_level(const log_level_t level) {
  
  log4c_priority_level_t log4c_level;
  
  switch(level) {
  case DEBUG:
    log4c_level = LOG4C_PRIORITY_DEBUG;
    break;
  case INFO:
    log4c_level = LOG4C_PRIORITY_INFO;
    break;
  case NOTICE:
    log4c_level = LOG4C_PRIORITY_NOTICE;
    break;
  case WARNING:
    log4c_level = LOG4C_PRIORITY_WARN;
    break;
  case ERROR:
    log4c_level = LOG4C_PRIORITY_ERROR;
    break;
  case CRITICAL:
    log4c_level = LOG4C_PRIORITY_CRIT;
    break;
  default:
    log4c_level = LOG4C_PRIORITY_NOTSET;
  }
  return log4c_level;
}

void log_level(const log_level_t level, const char* format, va_list args) {

  log4c_category_t* log_category = log4c_category_get(LOG4C_CATEGORY);
  
  char buffer[1024];
  vsnprintf(buffer, 1024, format, args);
  
  log4c_priority_level_t log4c_level = transform_log_level(level);
  log4c_category_log(log_category, log4c_level, "%s", buffer);
  va_end(args);
}
