
#include <string.h>
#include <curl/curl.h>
#include <json-c/json.h>

#include "api.h"
#include "api_common.h"
#include "api_error.h"
#include "logger.h"
#include "pki.h"


void register_account(char* email, char* password, char* fqdn) {
  log_info("Registering new account [%s] with email [%s]", fqdn, email);
  
  json_object* jquery = json_object_new_object();
  
  json_object* jemail = json_object_new_string(email);
  json_object_object_add(jquery, "email", jemail);
  
  json_object* jpassword = json_object_new_string(password);
  json_object_object_add(jquery, "password", jpassword);
  
  json_object* jfqdn = json_object_new_string(fqdn);
  json_object_object_add(jquery, "fqdn", jfqdn);

  const char* json_string = json_object_to_json_string(jquery);

  int status = callout("/register", json_string);

  if(status == 0) {
    log_info("Your account was created successfully.");
  } else {
    api_error_print_message("register", status);
  }
}


void setup_instance(char* email, char* password, char* fqdn, char* name, char* interface, char* tags) {
  
  log_info("Seting up instance..", fqdn, email);
  
  int key_success = generate_key(/*PRNG seed*/password);

  if(!key_success) {
    log_error("Your instance failed to setup. There was an issue during the key generation.");
    return;
  }
  
  json_object* jquery = json_object_new_object();
  
  char* public_key = read_public_key();
  
  json_object* jpublic_key = json_object_new_string(public_key);
  json_object_object_add(jquery, "publicKey", jpublic_key);
  
  json_object* jemail = json_object_new_string(email);
  json_object_object_add(jquery, "email", jemail);
  
  json_object* jpassword = json_object_new_string(password);
  json_object_object_add(jquery, "password", jpassword);
  
  json_object* jfqdn = json_object_new_string(fqdn);
  json_object_object_add(jquery, "fqdn", jfqdn);

  if(name != NULL) {
    json_object* jname = json_object_new_string(name);
    json_object_object_add(jquery, "name", jname);
  }

  if(interface != NULL) {
    json_object* jinterface = json_object_new_string(interface);
    json_object_object_add(jquery, "interface", jinterface);
  }

  if(tags != NULL) {  
    json_object* jtags = json_object_new_string(tags);
    json_object_object_add(jquery, "tags", jtags);
  }

  const char* json_string = json_object_to_json_string(jquery);

  int status = callout("/setup", json_string);

  if(status == 0) {
    log_info("Your instance is setup.");
  } else {
    api_error_print_message("register", status);
  }
}
