

#include <openssl/rsa.h>
#include <openssl/crypto.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/rand.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "pki.h"
#include "logger.h"

#define RSA_KEY_LENGTH 4096
#define PUBLIC_KEY_FILE_DELIMITER_SPACE 1000 // arbitrary
#define PRIVATE_KEY_PATH "rsa.priv"
#define PUBLIC_KEY_PATH  "rsa.pub"

int printPublicKey(RSA *key) {
  BIO *bio_out;
  bio_out = BIO_new_fp(stdout, BIO_NOCLOSE);
  PEM_write_bio_RSAPublicKey(bio_out, key);
  return 1;
}

int printPrivateKey(RSA *key) {
  BIO *bio_out;
  bio_out = BIO_new_fp(stdout, BIO_NOCLOSE);
  PEM_write_bio_RSAPrivateKey(bio_out, key, NULL, 0, 0, NULL, NULL);
  return 1;
}

void save_private_key(RSA* key) {
  FILE* key_file = fopen(PRIVATE_KEY_PATH, "w");
  PEM_write_RSAPrivateKey(key_file, key, NULL, 0, 0, NULL, NULL);
  fclose(key_file);
}

void save_public_key(RSA* key) {
  log_info("Storing new communication keys");
  FILE* key_file = fopen(PUBLIC_KEY_PATH, "w");
  PEM_write_RSAPublicKey(key_file, key);
  fclose(key_file);
}

void save_key(RSA* key) {
  save_private_key(key);
  save_public_key(key);
}

char* read_public_key() {
  FILE* key_file = fopen(PUBLIC_KEY_PATH, "r");
  int max_size =  RSA_KEY_LENGTH * 2 + PUBLIC_KEY_FILE_DELIMITER_SPACE;
  char* key_str = (char*) calloc(max_size, sizeof(char));
  fread(key_str, sizeof(char), max_size, key_file);
  fclose(key_file);
  return key_str;
}

int generate_key(char* prng_seed) {
  log_info("Generating a set of new communication keys");
  BIGNUM* exponent = BN_new();
  BN_set_word(exponent, RSA_F4);
  RAND_seed(prng_seed, sizeof(char) * strlen(prng_seed));
  RSA* key = RSA_new();
  int status = RSA_generate_key_ex(key, RSA_KEY_LENGTH, exponent, NULL);
  //RSA_check_key(key);
  //printPrivateKey(key);
  //printPublicKey(key);
  if(status) {
    save_key(key);
  }
  return status;
}

int sign(char* value, char* signature) {
  return 0;
}
