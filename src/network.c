
#include "network.h"
#include "hashmap.h"

static const char PUBLIC_IP_QUERY_URL[] = "https://my-ip.dn5.io";

int interface_struct_to_json(any_t json_obj_interfaces, any_t network_interface) {
  json_object* jinterfaces = (json_object*) json_obj_interfaces;
  network_interface_t* interface = (network_interface_t*) network_interface;

  json_object* jinterface = json_object_new_object();
  json_object* jinterface_name = json_object_new_string(interface->name);
  json_object_object_add(jinterface, "name", jinterface_name);

  json_object* jadresses = json_object_new_array();
  for(int i = 0 ; i < interface->addresses_count ; i++) {
    json_object* jaddress = json_object_new_string(interface->addresses[i]);
    json_object_array_add(jadresses, jaddress);
  }
  json_object_object_add(jinterface, "addresses", jadresses);

  json_object_array_add(jinterfaces, jinterface);
  return MAP_OK;
}

const char* network_description_to_json(const char* public_ip_address, map_t interfaces_map) {
  json_object* jobj = json_object_new_object();
  json_object* jip = json_object_new_string(public_ip_address);
  json_object_object_add(jobj, "publicIPAddress", jip);
  json_object* jinterfaces = json_object_new_array();
  json_object_object_add(jobj, "interfaces", jinterfaces);

  hashmap_iterate(interfaces_map, (PFany) &interface_struct_to_json, (any_t) jinterfaces);
  
  const char* json_string = json_object_to_json_string(jobj);
  return json_string;
}

char* inet_to_string(struct in_addr inet) {
  const char* address = inet_ntoa(inet);
  char* buffer = malloc((strlen(address)+1) * sizeof(char*));
  strcpy(buffer, address);
  return buffer;
}

const char* describe_network_to_json() {
  struct ifaddrs *ifaddr, *ifa;

  if(getifaddrs(&ifaddr) == -1) {
    perror("ERROR getifaddrs");
  }

  map_t interfaces_map = hashmap_new();

  for(ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
    if(ifa->ifa_addr != NULL) {
      //printf("%s ==> %s\n", ifa->ifa_name, inet_ntoa(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr));
      network_interface_t* interface;
      int found = hashmap_get(interfaces_map, ifa->ifa_name, (any_t *) &interface);
      if(found != MAP_OK) {
        log_info("new interface %s", ifa->ifa_name);
        interface = (network_interface_t*) malloc(sizeof(network_interface_t));
        interface->addresses_count = 0;
        interface->name = ifa->ifa_name; // TODO: strcpy
        hashmap_put(interfaces_map, ifa->ifa_name, (any_t) interface);
      
        interface->addresses = (char**)malloc(sizeof(char*));
        interface->addresses[0] = inet_to_string(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr);
        
      } else {
        interface->addresses = (char**)realloc(interface->addresses, (interface->addresses_count+1) * sizeof(char*));
        interface->addresses[interface->addresses_count] = inet_to_string(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr);
        log_info("existing interface %s, new ip %s", interface->name, interface->addresses[interface->addresses_count]);
      }
      interface->addresses_count ++;
      
    }
  }
  
  //char* public_ip_address = "0.0.0.0";
  char public_ip_address[MAX_IP_ADDRESS_BUFFER_SIZE];
  fetch_public_ip(public_ip_address);
  
  freeifaddrs(ifaddr);
  return network_description_to_json(public_ip_address, interfaces_map);
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, char *ip_address) {

  size_t char_count = size * nmemb;
  if((strlen(ip_address) + char_count) < MAX_IP_ADDRESS_BUFFER_SIZE) { // poor man's protection against overflow
    strncat(ip_address, (char*)ptr, size * nmemb);
    return char_count;
  }
  return 0;
}

void fetch_public_ip(char* ip_address) {
  CURL *curl;
  CURLcode res;
  ip_address[0] = '\0';
  /* get a curl handle */ 
  curl = curl_easy_init();
  log_info("Retrieving public IP address from [%s]", PUBLIC_IP_QUERY_URL);
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, PUBLIC_IP_QUERY_URL);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, ip_address);
 
    res = curl_easy_perform(curl);
    if(res != CURLE_OK) {
      log_error("fetch_public_ip() failed: %s\n", curl_easy_strerror(res));
    }
 
    curl_easy_cleanup(curl);
  }
}
