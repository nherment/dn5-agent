
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>

#include "dn5-agent.h"
#include "network.h"
#include "arguments.h"
#include "logger.h"
#include "hashmap.h"
#include "api.h"

/*
  
dn5-agent setup
  --email fubar@domain.tld
  --password *****
  --tags tag1,tag2,...
  --hostname the_public_hostname
  --interface public|<interface_name>
*/
void define_setup_command() {
  arguments_t* setup = malloc(sizeof(arguments_t));
  setup->verb = "setup";
  setup->count = 6;
  setup->arguments = malloc( setup->count * sizeof(argument_t) );
  
  setup->arguments[0].value = "--email";
  setup->arguments[0].required = TRUE;
  setup->arguments[0].type = ARG_TYPE_STRING;
  
  setup->arguments[1].value = "--password";
  setup->arguments[1].required = TRUE;
  setup->arguments[1].type = ARG_TYPE_STRING;
  
  setup->arguments[3].value = "--fqdn";
  setup->arguments[3].required = TRUE;
  setup->arguments[3].type = ARG_TYPE_STRING;
  
  setup->arguments[5].value = "--name";
  setup->arguments[5].required = FALSE;
  setup->arguments[5].type = ARG_TYPE_STRING;
  
  setup->arguments[4].value = "--interface";
  setup->arguments[4].required = FALSE;
  setup->arguments[4].type = ARG_TYPE_STRING;
  
  setup->arguments[2].value = "--tags";
  setup->arguments[2].required = FALSE;
  setup->arguments[2].type = ARG_TYPE_STRING;
  
  arguments_define(setup);
}

void define_register_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "register";

  cmd->count = 3;
  cmd->arguments = malloc( cmd->count * sizeof(argument_t) );
  
  cmd->arguments[0].value = "--fqdn";
  cmd->arguments[0].required = FALSE;
  cmd->arguments[0].type = ARG_TYPE_STRING;
  
  cmd->arguments[1].value = "--email";
  cmd->arguments[1].required = TRUE;
  cmd->arguments[1].type = ARG_TYPE_STRING;
  
  cmd->arguments[2].value = "--password";
  cmd->arguments[2].required = TRUE;
  cmd->arguments[2].type = ARG_TYPE_STRING;
  
  arguments_define(cmd);
}

void define_update_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "update";
  cmd->count = 2;
  cmd->arguments = malloc( cmd->count * sizeof(argument_t) );
  cmd->arguments[0].value = "--fqdn";
  cmd->arguments[0].required = FALSE;
  cmd->arguments[0].type = ARG_TYPE_STRING;
  cmd->arguments[1].value = "--interface";
  cmd->arguments[1].required = FALSE;
  cmd->arguments[1].type = ARG_TYPE_STRING;
  arguments_define(cmd);
}

void define_takedown_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "takedown";
  cmd->count = 0;
  arguments_define(cmd);
}

void define_status_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "status";
  cmd->count = 0;
  arguments_define(cmd);
}

void define_timeout_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "timeout";
  cmd->count = 0;
  arguments_define(cmd);
}

void define_eula_command() {
  arguments_t* cmd = malloc(sizeof(arguments_t));
  cmd->verb = "eula";
  cmd->count = 0;
  arguments_define(cmd);
}

void define_usage() {
  define_register_command();
  define_setup_command();
  define_update_command();
}


int main (int argc, char **argv, char** envp) {
  global_init();
  
  log_info("Started agent version %d.%d.%d", DN5_VERSION_MAJOR, DN5_VERSION_MINOR, DN5_VERSION_PATCH);

  define_usage();
  map_t args = arguments_parse(argc, argv);

  if(args == NULL) {
    return 1;
  }
  
  char* cmd;
  hashmap_get(args, "cmd", (any_t*)&cmd);
  //log_info("COMMAND %s", cmd);
  if(strcmp("register", cmd) == 0) {
    char *email, *password, *fqdn;
    hashmap_get(args, "--email", (any_t*)&email);
    hashmap_get(args, "--password", (any_t*)&password);
    hashmap_get(args, "--fqdn", (any_t*)&fqdn);
    register_account(email, password, fqdn);
  } else if(strcmp("setup", cmd) == 0) {
    char *email, *password, *fqdn, *name, *interface, *tags;
    hashmap_get(args, "--email", (any_t*)&email);
    hashmap_get(args, "--password", (any_t*)&password);
    hashmap_get(args, "--fqdn", (any_t*)&fqdn);
    hashmap_get(args, "--name", (any_t*)&name);
    hashmap_get(args, "--interface", (any_t*)&interface);
    hashmap_get(args, "--tags", (any_t*)&tags);
    setup_instance(email, password, fqdn, name, interface, tags);
  } else {
    log_error("Command [%s] is documented but not implemented", cmd);
    return 1;
  }
  //const char* json_string = describe_network_to_json();
  //printf("interfaces report: %s\n", json_string);
  global_cleanup();
  return 0;
}
