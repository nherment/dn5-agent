
#include "global.h"

bool global_init() {
  logger_init();
  curl_global_init(CURL_GLOBAL_ALL);
  return true;
}

bool global_cleanup() {
  logger_cleanup();
  curl_global_cleanup();
  return true;
}
