#!/bin/sh

# run npm install before
SCRIPTS_DIR=`dirname "$PWD/${0##}"`

cd $SCRIPTS_DIR
sh ./generate_documentation.sh

cd $SCRIPTS_DIR/../
scp -r docs/out/* dn5.io:./www/dn5.io/
