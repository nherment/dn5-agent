#!/bin/sh

# run npm install before
SCRIPTS_DIR=`dirname "$PWD/${0##}"`
cd $SCRIPTS_DIR/../

mkdir -p docs/out > /dev/null

cp docs/INDEX.md docs/out/INDEX.md
python scripts/markdown.py docs/INDEX.md docs/style.css docs/out/index.html

cp docs/EULA.md docs/out/
python scripts/markdown.py docs/EULA.md docs/style.css docs/out/eula.html

cp docs/LICENSE.md docs/out/
python scripts/markdown.py docs/LICENSE.md docs/style.css docs/out/license.html

cp docs/QUICK_START.md docs/out/
python scripts/markdown.py docs/QUICK_START.md docs/style.css docs/out/quick_start.html

