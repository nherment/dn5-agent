import markdown2
import os, sys


outfile = open(sys.argv[3], "w")
outfile.write("""<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <style type="text/css">
""")


cssin = open(sys.argv[2])
outfile.write(cssin.read())


outfile.write("""
    </style>
</head>

<body>
""")

mkin = open(sys.argv[1])
outfile.write(
    markdown2.markdown(
        mkin.read(),
        extras=[
            'fenced-code-blocks',
            'cuddled-lists',
            'tables',
            'code-friendly'
        ],
        safe_mode='escape'
    )
)

outfile.write("""</body>

</html>
""")


outfile.close()
