
## What is dn5.io ?

It is a dynamic DNS provider, designed to be extremely simple to setup.

Interaction with dn5.io is fully command line based. There is no web UI, yet.


## Documentation at [https://dn5.io](https://dn5.io) or [docs/](docs/).

