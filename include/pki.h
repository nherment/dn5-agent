
#pragma once

#include <openssl/rsa.h>

char* read_public_key();
int generate_key(char* prng_seed);
int sign(char* value, char* signature);
