#ifndef DN5_GLOBAL
#define DN5_GLOBAL

#include <stdlib.h>
#include <stdbool.h>
#include <curl/curl.h>
#include <json-c/json.h>
#include "logger.h"
#include "linked_list.h"
//#include "hashmap.h"

bool global_init();
bool global_cleanup();

#endif
