#ifndef DN5_NETWORK
#define DN5_NETWORK

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <curl/curl.h>
#include "global.h"

#define MAX_IP_ADDRESS_BUFFER_SIZE 256

typedef struct network_interface_t {
  char* name;
  char** addresses;
  int addresses_count;
} network_interface_t;

const char* describe_network_to_json();
void fetch_public_ip(char* ip_address);

#endif
