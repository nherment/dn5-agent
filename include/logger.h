#ifndef LOGGER
#define LOGGER

#include <log4c.h>

#define LOG4C_CATEGORY "dn5.agent"

typedef enum {
  DEBUG, INFO, NOTICE, WARNING, ERROR, CRITICAL
} log_level_t;

void logger_init();
void logger_cleanup();

void log_debug(const char* format, ...);
void log_info(const char* format, ...);
void log_notice(const char* format, ...);
void log_warning(const char* format, ...);
void log_error(const char* format, ...);
void log_critical(const char* format, ...);
void log_level(const log_level_t level, const char* format, va_list args);

#endif
