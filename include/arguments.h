#ifndef DN5_ARGUMENTS
#define DN5_ARGUMENTS

//#include "bool.h"
#include <bool.h>
#include "hashmap.h"

typedef enum argument_type_e {
  ARG_TYPE_BOOL,
  ARG_TYPE_STRING
} argument_type_e;

typedef struct argument_t {
  _Bool required;
  char* value;
  argument_type_e type;
} argument_t;

typedef struct arguments_t {
  char* verb;
  int count;
  argument_t* arguments;
} arguments_t;

void arguments_define(arguments_t* arguments);

map_t* arguments_parse(int argc, char** args);

void arguments_free();

#endif
