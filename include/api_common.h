
#pragma once

#define DN5_REMOTE_API_URL "http://localhost:8080"
#define MAX_BODY_SIZE 1000
#define MAX_HEADER_SIZE 1000

typedef struct http_header {
  char* key;
  char* value;
} http_header;

typedef struct http_response {
  long status_code;
  int header_count;
  http_header* headers;
  char* body;
} http_response;


//size_t parse_body(char *buffer, size_t size, size_t nmemb, http_response *response);
void post(char* url_path, const char* query, http_response* response);
int callout(char* url, const char* query);
