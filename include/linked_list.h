#ifndef LINKED_LIST
#define LINKED_LIST

#include <stdlib.h>
#include <stdbool.h>

typedef struct linked_list_node_t {
  void *element;
  struct linked_list_node_t *next;
  struct linked_list_node_t *previous;
} linked_list_node_t;


typedef struct linked_list_t {
  int length;
  linked_list_node_t *head;
  linked_list_node_t *tail;
} linked_list_t;

typedef bool (*linked_list_iterator)(void*);

linked_list_t* linked_list_create();

/** Append an element to the list */
void linked_list_push(linked_list_t* linked_list, void* element);

/** Removes the last element */
void* linked_list_pop(linked_list_t* linked_list);

/** Removes the first element */
void* linked_list_shift(linked_list_t* linked_list);

/** Prepend an element to the list */
void* linked_list_unshift(linked_list_t* linked_list);

void linked_list_destroy(linked_list_t* linked_list);

#endif
