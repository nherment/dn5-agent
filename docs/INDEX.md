# dn5.io

> dn5.io is currently a work in progress and this readme is a guideline for the
> development and implementation of features.
> You can get in touch on github
> [https://github.com/nherment/dn5-agent](https://github.com/nherment/dn5-agent)

### [Quick start](quick_start.html)

## What is dn5.io ?

It is a dynamic DNS provider, designed to be extremely simple to setup.

Interaction with dn5.io is fully command line based. There is no web UI, yet.


## How does it work ?

In a nutshell, there are clients (called agents) and the DNS server. Each agent
works with an account (1 account = 1 domain).

Most often, an agent represents a server and is able to regularly send the DNS
server its own IP (public or private network).
If it stops sending status updates, the agent will be considered dead and its
data will be removed from the DNS after a timeout. An agent can also self
terminate.

Agents first need to be registered with a master password. After that, they use
Public/Private keys to authenticate themselves.


## Install the agent


### Debian

```
wget https://dn5.io/dn5-io.gpg.key
apt-key add - < dn5-io_release.key

echo 'deb https://dn5.io/packages/debian jessie main' >> /etc/apt/sources.list.d/dn5.io.list

apt-get update
apt-get install dn5-agent
```


### Compile from source

```
# install cmake
apt-get install cmake
git clone git@github.com:nherment/dn5-agent.git
cd dn5-agent
./scripts/install.sh
```


## Register a new account

Before you can start using dn5.io, you need to reserve a domain and attach it
to your account.

```
dn5-agent register
  --fqdn example.dn5.at
  --email fubar@example.com
  --password *****
```

This command has no effect on the client machine. It's the equivalent to
registering on a website.

We provide all subdomains belonging to ``dn5.at`` so that you don't have to own
your own domain.

> Any registration needs to be validated by opening a link sent to the
> registered email address. Email validation has a 24 hours timeout, after which
> the FQDN is freed.


### Using your own domain

You can use your own domain or subdomain, in which case you need to use the
``postmaster`` email address of the second level domain. eg:

```
dn5-agent register
  --fqdn example.com
  --email postmaster@example.com
  --password *****
```

You will also need to relinquish your DNS to dn5.io nameservers.
These are:

```
ns1.dn5.io
ns2.dn5.io
```

#### sub-domain

If you would like to only delegate a subdomain to dn5.io, just change the FQDN
to that subdomain.

Setup the following records in your existing DNS zone:

```
sub.example.com.	48600	IN	NS	ns1.dn5.io.
sub.example.com.	48600	IN	NS	ns2.dn5.io.
```

> You still need to validate the account through the second level administrator
> email address matching your subdomain. For example, for the subdomain
> ``sub.example.com``, the email must be ``postmaster@example.com``.


## setup your first machine

### CMD: setup

Once you have registered your account, it is tied to a specific domain. From
each machine that you want managed by dn5.io, run the following command.

```
dn5-agent setup
  --email <fubar@domain.tld>
  --password <*****>
  --tags <tag1>,<tag2>,...
  --fqdn <the_public_fqdn>
  [--name <unique_instance_name>]
  [--interface <interface_name>]
```

This command will:
- validate that this email is registered to a FQDN on dn5.io
- validate that the email and password match
- warn you if the IP is already used by another machine in your account
- generate and locally save a unique private/public key pair for your machine
- validate that the ``--name`` is unique in the account
- validate that ``--fqdn`` belongs to that account. For example, if your
account registered the domain ``account.dn5.at``, a valid FQDN for your instance
could be ``srv1.account.dn5.at``
- send the first update command to dn5.io to make sure there is no error
- setup a cronjob to regularly update dn5.io

The ``--name`` is an id used to uniquely identify your instance. A random one
will be generated for you if you omit this option. It must be alphanumeric with
the following special characters allowed: ``_-()[]{}``.

By default, `--interface` is ``public``. Meaning dn5-agent resolves the public
IP of the machine by making a request to a public webserver
(https://my-ip.dn5.io). However, if you want the DNS server to serve an IP set
on a local interface, you can use that interface name instead:
``--interface en0``.

> The RSA key pair generated must be unique to this machine. If another machine
> uses the same key pair, this key pair may be blacklisted. A new ``setup``
> command must be executed to re-enable an instance.


## General usage

### CMD: update

During setup, the agent generates a cronjob that calls ``dn5-update`` every 5
minutes.
You can update that cronjob yourself with ``crontab -e``.

```
dn5-agent update
  [--interface <interface>]
  [--fqdn <fqdn>]
```

By default, the ``fqdn`` and ``interface`` updated are the ones used during
the setup.


### CMD: takedown

If you know that your server or service is going to be unavailable and you would
like to officially decommission it without waiting for a timeout, you can call
the following command:

```
dn5-agent takedown
```

### CMD: status

Prints out an overview of all your instances.

```
dn5-agent status
  [--tags <tag1>,<tag2>,...]
  [--status active|timedout|...]
  [--columns iflrds]
```

The ``--tags`` option is to filter the instances by tag (with an AND operator
between each tag).

The ``--status`` option is to filter the instances by status.
The ``--columns`` option is a list of characters to select which columns to display (see below).

Here is a sample output:

```
| Instance |        First seen       |        Last seen        |    Reported IP    |           DNS records          |   Status  |
|:--------:|:-----------------------:|:-----------------------:|:-----------------:|:------------------------------:|:---------:|
| server1  | 2015-10-04 12:34:40.123 | 2016-02-04 02:12:40.123 | en0_ipv4:10.0.0.1 | A:srv1.account.dn5.at:10.0.0.1 | active    |
| server2  | 2015-10-04 12:34:40.123 | 2016-02-04 02:12:40.123 | en0_ipv4:10.0.0.2 | A:srv2.account.dn5.at:10.0.0.2 | active    |
| server3  | 2015-10-04 12:34:40.123 | 2016-02-01 02:12:40.123 | en0_ipv4:10.0.0.3 | A:srv3.account.dn5.at:10.0.0.3 | timedout  |
```

For which each column is explained in the following table:

| Column      | Option | Description                                                         | Possible values                             | Example                                                                  |
|:-----------:|:-------------:|:-------------------------------------------------------------------:|:-------------------------------------------:|:------------------------------------------------------------------------:|
| Instance    | i      | The instance name defined during setup                              | characters allowed: a-z, A-Z, 0-9, _-()[]{} | foobar[1]                                                                |
| First seen  | f      | The first time the instance was seen (UTC)                          | YYYY:MM:DD HH:mm:ss.ddd                     | 2016-02-04 02:12:40.123                                                  |
| Last seen   | l      | The last time the instance was seen (UTC)                           | YYYY:MM:DD HH:mm:ss.ddd                     | 2016-02-04 02:12:40.123                                                  |
| Reported IP | r      | A line-break separated list of reported interface & IP              | <interface>_ipv<4|6>:<ip_address>           | public_ipv4:123.123.123.123 en0_ipv4:10.0.0.1                            |
| DNS records | d      | A line-break separated list of DNS records mapping to this instance | <domain> <ttl> <type> <target>              | srv1.account.dn5.at 0 A 123.123.123 en0.srv1.account.dn5.at 0 A 10.0.0.1 |
| Status      | s      | The current status of the instance (as seen by the server)          | active,timedout,banned,inactive             | active                                                                   |


### CMD: timeout

Reads and updates timeout rules.

Timeouts rules can be set per instance, per tag or per domain.

```
dn5-agent timeout
  [--timeout <seconds>]
  [--delete]
  [--fqdns <fqdn1>,<fqdn2>,...]
  [--tags <tag1>,<tag2>,...]
  [--instances <name1>,<name2>,...]
```

- ``--timeout`` will set the timeout in seconds
- ``--delete`` will delete an existing timeout rule
- ``--fqdn`` sets for which domains to create/update/delete a timeout rule
(comma sepaeared list)
- ``--tags`` sets for which instance tags to create/update/delete a timeout rule
(comma separated list)
- ``--instances`` sets for which instances to create/update/delete a timeout
rule (comma separated list)

> Omitting both the ``--timeout`` and ``--delete`` options will display the
current timeouts rules for your instances.

#### Example

Here is an example of an output of the timeout command:

```
> dn5-agent timeout
account-timeout: 5 minutes
|        Domains       |         Tags        | Instances | Timeout delay |
|:--------------------:|:-------------------:|:---------:|:-------------:|
| account.dn5.at       |                     |           | 10 minutes    |
| account.dn5.at       | database,production |           | 60 minutes    |
| zone1.account.dn5.at |                     | srv1      | 5 minutes     |
```

- The first line overrides the account timeout to 10 minutes. You cannot
currently override your account timeout to a lower value (less than 5 minutes).
We are working hard to give you full freedom over timeouts. This feature is in
the pipeline.
- The second line stipulates that any instance matching both tags ``database``
AND ``production`` will have a timeout delay of 60 minutes.
- The third line stipulates that the timeout for instance ``srv1`` and FQDN
``zone1.account.dn5.at`` is 5 minutes.

> The rules are always displayed ordered from least restrictive on top and most
> restrictive at the bottom.

The rules are evaluated linearly from most restrictive to least restrictive. In
the example above, it means that an instance with name ``srv1``, FQDN
``zone1.account.dn5.at`` and tags ``database,production`` will have a timeout of
5 minutes as there is only one instance named ``srv1``.

The order for each rule attribute, from most to least restrictive is: ``name`` >
``fqdn`` > ``tag`` > ``account``.



### CMD: eula

Prints out the latest End-user license agreement.

```
dn5-agent eula
```

## Rate limitation

Most commands are rate limited to 1 per 1 minute per public IP and 100 per
minute per account. This is to prevent abuse and bruteforce attacks.

An intance or an account that violates the rate limitation will first be banned
for a delay of 60 minutes. The ban delay will exponentially increase up to 24
hours. After a 24 hour ban has ended, the instance or account will enter a
provisional period of 24 hours where any violation will result in another 24
hours ban.

We reserve the right to disable accounts without notice, at any time for any
reason.

