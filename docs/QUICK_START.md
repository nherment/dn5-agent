
# Quick start

### Debian

As root, install the agent:

```
#> wget https://dn5.io/dn5-io.gpg.key
#> apt-key add - < dn5-io.gpg.key

#> echo 'deb https://dn5.io/packages/debian jessie main' >> /etc/apt/sources.list.d/dn5.io.list

#> apt-get update
#> apt-get install dn5-agent
```


Setup your dynamic DNS on your server:

```
$> dn5-agent register \
    --fqdn <your_subdomain>.dn5.at \
    --email <your_email_address> \
    --password <your_password>

$> dn5-agent setup \
    --email <your_email_address> \
    --password <your_password> \
    --fqdn <your_subdomain>.dn5.at
```

Check that the FQDN resolves to your server:

```
ping <your_subdomain>.dn5.at
```


## Good to know

- The password is only necessary when doing administrative tasks (setting up a
server, etc). The agent sets up private/public key on your server and uses
that to authenticate any requests to dn5.io.

- This quick start sets up a CRON job to call ``dn5-agent setup`` at regular
intervals.