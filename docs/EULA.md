
Terms of use
============

This is a legal agreement between the person registering for a dn5.io account
("You" or "you") and the operators of dn5.io. ("We", "us"). This agreement
governs your use of the dn5.io service ("the service").

Client code
-----------

1. You may only access the service using unmodified dn5.io client code which we
have distributed. Compiling the source code to produce binaries does not
constitute "modification" for the purpose of this clause.

2. You may use the dn5.io client code for the sole purpose of accessing the
service.

Privacy
-------

3. We may send you email concerning the service or your account at any time.

4. We will not sell your email address to anyone or share it with another
purpose than providing the service.

5. We may provide information concerning your account and your use of the service
to 3rd parties, at our sole discretion, if
- It is requested by law enforcement authorities.
- We have reason to believe that it is in your interest that we provide such
information.
- It is necessary in order to keep the service running, or to diagnose or fix
problems with the service or another network.

6. Use of the service is at your own risk.

THE SERVICE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL I BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SERVICE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Miscellaneous
-------------

7. This agreement will be governed by the laws of Ireland.

8. We may update these Terms and Conditions at any time, but you will not be
bound by any updates until 30 days after we send you an email notifying you of
the changes.
